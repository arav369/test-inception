package webservice;

import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;

import org.codehaus.jackson.map.ObjectMapper;

import database.RenterDBAccessor;
import entity.Renter;

@Path("renter")
public class RenterResource {

	@GET
	@Path("{id}")
	@Produces("application/json")
	public String getRenter(@PathParam("id") String renterId) {
		Renter renter = null;
		try {
			if (renterId == null) {
				return "Invalid Renter ID";
			}
			renter  = RenterDBAccessor.getRenter(renterId);
			if (renter != null) {
				return new ObjectMapper().writeValueAsString(renter);
			}		
			return null;
		} catch(Exception e) {
			return e.getMessage();
		}
		}
	
	@POST
	@Consumes("application/json")
	public String createRenter(Renter renter) {
		return null;
	}
	
	@PUT
	@Consumes("application/json")
	public String updateRenter(Renter renter) {
		return null;
	}

	@DELETE
	@Path("{id}")
	public String deleteRenter(@PathParam("id") String renterId) {
		return null;
	}
	
}
