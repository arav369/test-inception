package webservice;

import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;

import entity.Product;

@Path("product")
public class ProductResource {

	@GET
	@Produces("application/json")
	public String getProductById(@PathParam("id") String productId) {
		return null;
	}
	
	@POST
	@Consumes("application/json")
	public String createProduct(Product product) {
		return null;
	}
	
	@PUT
	@Consumes("application/json")
	public String updateProduct(Product product) {
		return null;
	}

	@DELETE
	public String deleteProduct(@PathParam("id") String productId) {
		return null;
	}

}
