package entity;

/*
 * CREATE TABLE RENTER (
RENTER_ID INTEGER AUTO_INCREMENT,
FIRST_NAME VARCHAR(100),
LAST_NAME VARCHAR(100),
ADDRESS VARCHAR(500),
EMAIL_ID VARCHAR(50),
PHONE_NUMBER INTEGER,
RATINGS INTEGER,
USERNAME VARCHAR(20),
IMAGE_LOCATION VARCHAR(200),
PRIMARY KEY (RENTER_ID)
);
 */
public class Renter {
	
	private String id;
	private String firstName;
	private String lastName;
	private Address address;
	private String email;
	private String phone;
	private int ratings;
	private String userName;
	private String imageURI;
	
	public Renter() {
		super();
	}

	public Renter(String id, String firstName, String lastName, Address address, String email, String phone,
			int ratings, String userName, String imageURI) {
		super();
		this.id = id;
		this.firstName = firstName;
		this.lastName = lastName;
		this.address = address;
		this.email = email;
		this.phone = phone;
		this.ratings = ratings;
		this.userName = userName;
		this.imageURI = imageURI;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public Address getAddress() {
		return address;
	}

	public void setAddress(Address address) {
		this.address = address;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public int getRatings() {
		return ratings;
	}

	public void setRatings(int ratings) {
		this.ratings = ratings;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getImageURI() {
		return imageURI;
	}

	public void setImageLocation(String imageURI) {
		this.imageURI = imageURI;
	}
}
