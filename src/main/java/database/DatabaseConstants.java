package database;

public class DatabaseConstants {

	public static final String GET_RENTER_SQL = "SELECT * FROM RENTER WHERE %s = '%s'";
	public static final String CREATE_RENTER_SQL = 
			"INSERT INTO RENTER "
			+ "(FIRST_NAME, LAST_NAME, ADDRESS, EMAIL_ID, PHONE_NUMBER, RATINGS, USERNAME, IMAGE_LOCATION)"
			+ "VALUES (%s, %s, %s, %s, %s, %d, %s, %s)";
}