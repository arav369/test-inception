package database;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import org.codehaus.jackson.map.ObjectMapper;

import core.TestHarness;
import entity.Address;
import entity.Renter;
import static database.DatabaseConstants.GET_RENTER_SQL;
import static database.DatabaseConstants.CREATE_RENTER_SQL;

/*
 * mysql://bf1189107c2cfa:c44b7706@us-cdbr-azure-central-a.cloudapp.net/TestInceptionDB
 */
public class RenterDBAccessor {

	public static Renter getRenter(String renterId) throws Exception {
		Renter renter = null;
		ResultSet rs = null;
		Connection conn = null;
		Statement stmt = null;
		ObjectMapper mapper = new ObjectMapper();
		try {
			String sql = String.format(GET_RENTER_SQL, "RENTER_ID", renterId);
			conn = DatabaseUtils.getConnection();
			stmt = conn.createStatement();
			rs = stmt.executeQuery(sql);
			while(rs.next()){
				String firstName = rs.getString("FIRST_NAME");
				String lastName = rs.getString("LAST_NAME");
				String addressStr = rs.getString("ADDRESS");
				String email = rs.getString("EMAIL_ID");
				String phone = rs.getString("PHONE_NUMBER");
				int ratings = rs.getInt("RATINGS");
				String userName = rs.getString("USERNAME");
				String imageLoc = rs.getString("IMAGE_LOCATION");

				Address address = mapper.readValue(addressStr, Address.class);
				renter = new Renter (renterId, 
						firstName, 
						lastName, 
						address, 
						email, 
						phone,
						ratings, 
						userName, 
						imageLoc);
			}
			if (renter == null) {
				throw new Exception ("Renter with id '" + renterId + "' not found");
			}
			return renter;

		} catch(Exception e) {
			System.err.println(e.getMessage());
			throw e;
		}
		finally {
			DatabaseUtils.close(conn, stmt, rs);
		}
	}	

	public static Renter createRenter(Renter renter) throws Exception {
		Renter createdRenter = null;
		Connection conn = null;
		Statement stmt = null;
		ObjectMapper mapper = new ObjectMapper();
		try {
			String addressString = new ObjectMapper().writeValueAsString(renter.getAddress());
			String sql = String.format(CREATE_RENTER_SQL, renter.getFirstName(), 
										renter.getLastName(), addressString,
										renter.getEmail(), renter.getPhone(), 
										renter.getRatings(), renter.getUserName(), 
										renter.getImageURI());	
			conn = DatabaseUtils.getConnection();
			stmt = conn.createStatement();
			int rows = stmt.executeUpdate(sql);
			if (rows == 1) {
				System.out.println("Renter inserted into database");	
				ResultSet generatedKeys = stmt.getGeneratedKeys();
				if (generatedKeys.next()) {
	                String renterId = generatedKeys.getString(1);
	                createdRenter = getRenter(renterId);
	            }
	            else {
	                throw new SQLException("Creating renter failed, no ID obtained.");
	            }
			} else {
				System.out.println("Error creating renter");
			}
		} catch(Exception e) {
			System.err.println(e.getMessage());
			throw e;
		}
		finally {
			DatabaseUtils.close(conn, stmt, null);
		}
		return createdRenter;
	}
	
	public static void main(String[] args) throws Exception {
		
		Renter renter = TestHarness.getTestRenter();
		Renter createdRenter = createRenter(renter);
		
		System.out.println(new ObjectMapper().writeValueAsString(createdRenter));
		
	}
}
