package core;

import java.util.Random;

import entity.Address;
import entity.Renter;

public class TestHarness {

	public static Renter getTestRenter() {
		
		Renter renter = new Renter();
		
		int random = new Random().nextInt(10000);
		renter.setFirstName("FirstName" + random);
		renter.setLastName("LastName" + random);
		renter.setAddress(new Address("Line1", "Line2", "San Jose", "CA", "USA", "95134"));
		renter.setEmail("test@gmail.com");
		renter.setPhone("12345678");
		renter.setImageLocation("/images/renter/122.jpg");
		renter.setUserName("test_renter");
		renter.setRatings(3);
		
		return renter;
	}
}
